.PHONY: all

all: a.exe

a.exe: NeuralNet.o main.o
	gcc -o $@ $^

NeuralNet.o: NeuralNet.c
	gcc -c -o $@ $<

main.o: main.c
	gcc -c -o $@ $<
