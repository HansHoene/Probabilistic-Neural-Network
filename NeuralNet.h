#ifndef _NEURALNET_H_
#define _NEURALNET_H_

typedef struct {
    unsigned int        numInputs;
    double              *inputs;            // the inputs to the weight matrix
    
    unsigned int        numOutputs;
    double              *biases;            // bias for each output
    double              *netSums;           // the net sums for each output neuron (the outputs from the weight matrix)
    double              *outputs;           // the outputs
    double              *dErr_dNet;         // the change in the total error with respect to each net sum (next layer sets this array during backpropogation)
    
    double              **weightMatrix;     // 1st dimension is input index, 2nd dimension in output index
} Layer;

typedef struct {
    unsigned int        numLayers;
    Layer               *layers;
} NeuralNet;

/*
    Purpose: randomly initialise a neural network
    Arguments:
        NeuralNet     *nn       - neural network to initialise
        unsigned int  numLayers - number of layers
        unsigned int  numInputs - # of inputs
        unsigned int  ...       - number of output neurons per layer
*/
void InitNeuralNet(NeuralNet *nn, int numLayers, unsigned int numInputs, ...);

/*
    Purpose: make a prediction using the neural network
    Arguments:
        NeuralNet  *nn       - neural network to use
        double     inputs[]  - inputs to propogate into network
        double     outputs[] - location to store network's outputs
*/
void Predict(NeuralNet *nn, double inputs[], double outputs[]);

/*
    Purpose: alter weights after a prediction
    Arguments:
        NeuralNet  *nn            - neural network to train
        double     learnRate      - learning rate of the network (between 0 and 1)
        double     dErr_dOutput[] - the change in error in respect to each output
    
    For this training function to work, you must have previously predicted a set
    of outputs.  In other words, you must have made a preceding call to the
    Predict function.
*/
void Train(NeuralNet *nn, double learnRate, double dErr_dOutput[]);

/*
    Purpose: free all dynamically-allocated data in neural network
    Arguments:
        NeuralNet  *nn - neural network to free
*/
void FreeNeuralNet(NeuralNet *nn);

#endif
