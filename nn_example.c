/*
    Hans-Edward Hoene
    4/9/2018
    
    This file contains a sample neural network.  This neural network has two
    inputs and two outputs.  In addition, there is one hidden layer with two
    neurons.  The outputs, y2 and y3, should represent the OP1 and OP2 of the
    inputs, respectively.
    
    The Neural Network:
    x0 and x1 are the two inputs.
    net0 and net1 are the net sums going into the two neurons of the hidden layer
    y0 and y1 are the activation functions applied to net0 and net1, respectively
    net0 = (x0 * w0) + (x1 * w2)
    net1 = (x0 * w1) + (x1 * w3)
    
    net2 and net3 are the net sums from the hidden layer's weighted outputs
    y2 and y3 are the activation functions applied to net2 and net3, respectively
    net2 = (y0 * w4) + (y1 * w6)
    net3 = (y0 * w5) + (y1 * w7)
    
    y = 1 / (1 + pow(e, -(net))) = 1 / (1 + exp(-(net)))
    
    Also, de3 = partial derivative of error with respect to net3
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define ERROR_MAX 0.005
#define ALPHA 0.01

#define LEARN_RATE 1

#define INPUT_MIN 0
#define INPUT_MAX 1000

#define OP_1(a, b) ((a) || (b) ? 1 : 0)
#define OP_2(a, b) ((((a) && !(b)) || (!(a) && (b))) ? 1 : 0)

#define OP1(a, b) OP_1((a) % 100, (b) % 100)
#define OP2(a, b) OP_2((a) % 100, (b) % 100)

// get a random number
int RandomInput();

double RandomWeight();

void Predict(int x[], double bias[], double w[], double net[], double y[]);

void Train(int x[], double w[], double de[], double y[], int expected[]);

int main() {
    int x[2];
    double w[8];
    double net[4], y[4];
    double de[4];
    
    double bias[4];
    
    int expected[2];
    
    double avgErr;
    int i;
    
    double temp;
    
    // random seed
    srand(time(NULL));
    
    // assign random weights and biases
    for (i = 0; i < 8; i++) {
        w[i] = RandomWeight();
    }
    for (i = 0; i < 4; i++) {
        bias[i] = 0.01;
    }
    
    i = 0;
    do {
        // get random inputs
        x[0] = ((rand() % 4) == 0) ? 0 : RandomInput();
        x[1] = ((rand() % 4) == 0) ? 0 : RandomInput();
        
        // predict
        Predict(x, bias, w, net, y);
        
        // get expected results
        expected[0] = OP1(x[0], x[1]);
        expected[1] = OP2(x[0], x[1]);
        
        // update exponential rolling average error
        temp = 0.5 * (((expected[0] - y[2]) * (expected[0] - y[2])) + ((expected[1] - y[3]) * (expected[1] - y[3])));
        if (i == 0) {
            avgErr = temp;
        } else {
            avgErr = ((1 - ALPHA) * avgErr) + (ALPHA * temp);
        }
        ++i;
        
        // print results
        /*
                               Weights                 Inputs        Weighted Sum              Outputs
        Hidden Layer  | ----w0---- ----w2---- | x | ----x0---- | = | ----net0---- |   =>   | ----y0---- |
                      | ----w1---- ----w3---- |   | ----x1---- |   | ----net1---- |   =>   | ----y1---- |
        
                               Weights                 Inputs        Weighted Sum              Outputs
        Output Layer  | ----w4---- ----w6---- | x | ----y0---- | = | ----net2---- |   =>   | ----y2---- |
                      | ----w5---- ----w7---- |   | ----y1---- |   | ----net3---- |   =>   | ----y3---- |
        
        Expected Outputs = | ----##---- |    Error = | ----##---- |    Rolling Exponential Error = #
                           | ----##---- |            | ----##---- |
        
        */
        printf("Iteration: %d\n", i);
        printf("                       Weights                 Inputs        Weighted Sum              Outputs\n");
        printf("Hidden Layer  | %2.7lf %2.7lf | x | %10d | = | %2.7lf |   =>   | %2.7lf |\n", w[0], w[2], x[0], net[0], y[0]);
        printf("              | %2.7lf %2.7lf |   | %10d |   | %2.7lf |   =>   | %2.7lf |\n", w[1], w[3], x[1], net[1], y[1]);
        printf("\n");
        printf("                       Weights                 Inputs        Weighted Sum              Outputs\n");
        printf("Output Layer  | %2.7lf %2.7lf | x | %2.7lf | = | %2.7lf |   =>   | %2.7lf |\n", w[4], w[6], y[0], net[2], y[2]);
        printf("              | %2.7lf %2.7lf |   | %2.7lf |   | %2.7lf |   =>   | %2.7lf |\n", w[5], w[7], y[1], net[3], y[3]);
        printf("\n");
        printf("Expected Outputs = | %10d |    Error = | %2.7lf |    Rolling Exponential Error = %lf\n", expected[0], 0.5 * (expected[0] - y[2]) * (expected[0] - y[2]), avgErr);
        printf("                   | %10d |            | %2.7lf |\n", expected[1], 0.5 * (expected[1] - y[3]) * (expected[1] - y[3]));
        printf("\n\n");
        
        // backpropogate
        Train(x, w, de, y, expected);
        
    } while ((avgErr > ERROR_MAX) || (i < 1000));
    
    printf("\n\nFinal Weights:\n");
    for (i = 0; i < 8; i++) {
        printf("Weight[%d] = %lf\n", i, w[i]);
    }
    printf("\nFinal Biases:\n");
    for (i = 0; i < 4; i++) {
        printf("Bias[%d] = %lf\n", i, bias[i]);
    }
    printf("\n\n");
    
    while (1) {
        printf("Enter two numbers: ");
        i = 2 - scanf("%d%d", &x[0], &x[1]);
        
        if (i) {
           printf("Error!");
           return 0;
        }
        
        Predict(x, bias, w, net, y);
        printf("OR = %lf\nXOR=%lf\n\n", y[2], y[3]);
    }
    
    return 0;
}

int RandomInput() {
    return (rand() % (INPUT_MAX + 1 - INPUT_MIN)) + INPUT_MIN;
}

double RandomWeight() {
    #define PRECISION 4
    int mod;
    
    mod = pow(10, PRECISION);
    return (double)(rand() % mod) / (double)mod;
}

void Predict(int x[], double bias[], double w[], double net[], double y[]) {
    int temp;
    
    if (x[1] < x[0]) {
        temp = x[0];
        x[0] = x[1];
        x[1] = temp;
    }
    
    // predict hiden layer outputs
    net[0] = (x[0] * w[0]) + (x[1] * w[2]) + bias[0];
    net[1] = (x[0] * w[1]) + (x[1] * w[3]) + bias[1];
    y[0] = 1 / (1 + exp(-net[0]));
    y[1] = 1 / (1 + exp(-net[1]));
    
    // predict output layer outputs
    net[2] = (y[0] * w[4]) + (y[1] * w[6]) + bias[2];
    net[3] = (y[0] * w[5]) + (y[1] * w[7]) + bias[3];
    y[2] = 1 / (1 + exp(-net[2]));
    y[3] = 1 / (1 + exp(-net[3]));
}

void Train(int x[], double w[], double de[], double y[], int expected[]) {
    /* to update weight, subtract the partial derivate of error in respect to the weight */
    
    // initialise backpropogation (chain rule, find partial derivate of error in respect to each net sum)
    de[2] = (y[2] - expected[0]) * y[2] * (1 - y[2]);
    de[3] = (y[3] - expected[1]) * y[3] * (1 - y[3]);
    
    // backpropogate
    de[0] = ((de[2] * w[4]) + (de[3] * w[5])) * y[0] * (1 - y[0]);
    de[1] = ((de[2] * w[6]) + (de[3] * w[7])) * y[1] * (1 - y[1]);
    w[4] -= LEARN_RATE * de[2] * y[0];
    w[5] -= LEARN_RATE * de[3] * y[0];
    w[6] -= LEARN_RATE * de[2] * y[1];
    w[7] -= LEARN_RATE * de[3] * y[1];
    
    // backpropogate again (last layer: no need to calculate de's of previous layer)
    w[0] -= LEARN_RATE * de[0] * x[0];
    w[1] -= LEARN_RATE * de[1] * x[0];
    w[2] -= LEARN_RATE * de[0] * x[1];
    w[3] -= LEARN_RATE * de[1] * x[1];
}

