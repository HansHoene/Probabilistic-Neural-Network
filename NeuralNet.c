#include "NeuralNet.h"

#include <stdarg.h>     // variadic functions
#include <stdlib.h>     // malloc
#include <time.h>
#include <math.h>

void InitNeuralNet(NeuralNet *nn, int numLayers, unsigned int numInputs, ...) {
    va_list numOutputsPerLayer;
    unsigned int i;
    unsigned int j, k;
    
    srand(time(NULL));
    
    // get space for layers
    nn->numLayers = numLayers;
    nn->layers = (Layer *)malloc(sizeof(Layer) * numLayers);
    
    // initialise each layer
    va_start(numOutputsPerLayer, numLayers);
    for (i = 0; i < numLayers; i++) {
        // initialise inputs portion
        if (i == 0) {
            nn->layers[i].numInputs = numInputs;
        } else {
            nn->layers[i].numInputs = nn->layers[i - 1].numOutputs;
        }
        nn->layers[i].inputs = (double *)malloc(sizeof(double) * nn->layers[i].numInputs);
        
        // initialise output portion
        nn->layers[i].numOutputs = va_arg(numOutputsPerLayer, unsigned int);
        nn->layers[i].biases    = (double *)malloc(sizeof(double) * nn->layers[i].numOutputs);
        nn->layers[i].netSums   = (double *)malloc(sizeof(double) * nn->layers[i].numOutputs);
        nn->layers[i].outputs   = (double *)malloc(sizeof(double) * nn->layers[i].numOutputs);
        nn->layers[i].dErr_dNet = (double *)malloc(sizeof(double) * nn->layers[i].numOutputs);
        
        // initialise weight matrix
        nn->layers[i].weightMatrix = (double **)malloc(sizeof(double *) * nn->layers[i].numInputs);
        for (j = 0; j < nn->layers[i].numInputs; j++) {
            nn->layers[i].weightMatrix[j] = (double *)malloc(sizeof(double) * nn->layers[i].numOutputs);
        }
        
        // assign random biases and weights
        for (j = 0; j < nn->layers[i].numOutputs; j++) {
            nn->layers[i].biases[j] = 0.0;
        }
        for (j = 0; j < nn->layers[i].numInputs; j++) {
            for (k = 0; k < nn->layers[i].numOutputs; k++) {
                (nn->layers[i].weightMatrix[j])[k] = (rand() % 100000) / (double)100000;
            }
        }
    }
    va_end(numOutputsPerLayer);
}

void Predict(NeuralNet *nn, double inputs[], double outputs[]) {
    unsigned int i;
    unsigned int j, k;
    double *inputLocation;
    
    // process each layer
    for (i = 0; i < nn->numLayers; i++) {
        // get inputs
        if (i == 0) {
            inputLocation = inputs;
        } else {
            inputLocation = nn->layers[i - 1].outputs;
        }
        for (j = 0; j < nn->layers[i].numInputs; j++) {
            nn->layers[i].inputs[j] = inputLocation[j];
        }
        
        // get each output's net sum and output value
        for (j = 0; j < nn->layers[i].numOutputs; j++) {
            nn->layers[i].netSums[j] = nn->layers[i].biases[j];
            for (k = 0; k < nn->layers[i].numInputs; k++) {
                nn->layers[i].netSums[j] += nn->layers[i].inputs[k] * nn->layers[i].weightMatrix[k][j];
            }
            nn->layers[i].outputs[j] = 1 / (1 + exp(-(nn->layers[i].netSums[j])));
            
            // if last layer, copy output back
            if (i == (nn->numLayers - 1)) {
                outputs[j] = nn->layers[i].outputs[j];
            }
        }
    }
}

void Train(NeuralNet *nn, double learnRate, double dErr_dOutput[]) {
    unsigned int i;
    unsigned int j, k;
    int flag;
    
    // set dErr_dNet of last layer
    for (i = 0; i < nn->layers[nn->numLayers - 1].numOutputs; i++) {
        nn->layers[nn->numLayers - 1].dErr_dNet[i] = dErr_dOutput[i] * nn->layers[nn->numLayers - 1].outputs[i] * (1 - nn->layers[nn->numLayers - 1].outputs[i]);
    }
    
    // loop through every layer in reverse order
    flag = (nn->layers > 0);
    for (i = (nn->numLayers - 1); flag; --i) {
        // if not last layer, set dErr_dNet values of previous layer
        if (i > 0) {
            for (j = 0; j < nn->layers[i - 1].numOutputs; j++) {
                nn->layers[i - 1].dErr_dNet[j] = 0;
                for (k = 0; k < nn->layers[i].numOutputs; k++) {
                    nn->layers[i - 1].dErr_dNet[j] += nn->layers[i].dErr_dNet[k] * nn->layers[i].weightMatrix[j][k];
                }
                nn->layers[i - 1].dErr_dNet[j] *= nn->layers[i - 1].outputs[j] * (1 - nn->layers[i - 1].outputs[j]);
            }
        }
        
        // update weights for current layer based on dErr_dNet values
        for (j = 0; j < nn->layers[i].numInputs; j++) {
            for (k = 0; k < nn->layers[i].numOutputs; k++) {
                nn->layers[i].weightMatrix[j][k] -= learnRate * nn->layers[i].dErr_dNet[k] * nn->layers[i].inputs[j];
            }
        }
        
        // end loop?
        if (i == 0) {
            flag = 0;
        }
    }
}

void FreeNeuralNet(NeuralNet *nn) {
    unsigned int i;
    unsigned int j, k;
    
    for (i = 0; i < nn->numLayers; i++) {
        // free weight matrix
        for (j = 0; j < nn->layers[i].numInputs; j++) {
            free(nn->layers[i].weightMatrix[j]);
        }
        free(nn->layers[i].weightMatrix);
        
        // free output portion
        free(nn->layers[i].biases);
        free(nn->layers[i].netSums);
        free(nn->layers[i].outputs);
        free(nn->layers[i].dErr_dNet);
        nn->layers[i].numOutputs = 0;
        
        // free input portion
        free(nn->layers[i].inputs);
        nn->layers[i].numInputs = 0;
    }
    
    free(nn->layers);
    nn->numLayers = 0;
}
