#include "NeuralNet.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define ERROR_MAX 0.001
#define ALPHA 0.01

#define LEARN_RATE 0.4

#define INPUT_MIN 0
#define INPUT_MAX 20

#define OP1(a, b) ((a) || (b) ? 1 : 0)
#define OP2(a, b) ((((a) && !(b)) || (!(a) && (b))) ? 1 : 0)
#define OP3(a, b) (((a) && (b)) ? 1 : 0)

// get a random number
int RandomInput(unsigned int i);

void TrainMyNetwork(NeuralNet *nn, double maxError, double alpha, unsigned int minIterations);
void TestMyNetwork(NeuralNet *nn);

// vary learn rate based on iteration
double DetermineLearnRate(unsigned int i);

int main() {
    NeuralNet nn;
    
    // create neural network
    InitNeuralNet(&nn,
                    3,      // 3 layers; 1 output layer, 2 hidden layers
                    2,      // 2 inputs (into first hidden layer)
                    5,      // five neurons/outputs in first hidden layer
                    4,      // four neurons/outputs in second hidden layer
                    3);     // three neurons/outputs in output layer
    
    // train my network
    TrainMyNetwork(&nn, ERROR_MAX, ALPHA, 10000);
    
    // test my network
    TestMyNetwork(&nn);
    
    FreeNeuralNet(&nn);
    
    return 0;
}

void TrainMyNetwork(NeuralNet *nn, double maxError, double alpha, unsigned int minIterations) {
    unsigned int i;
    int x[2];
    double in[2];
    double out[3];
    
    int temp;
    
    double errVals[3];
    
    double avgErr;
    
    i = 0;
    avgErr = 100;
    do {
        // get random inputs
        x[0] = RandomInput(i);
        x[1] = RandomInput(i);
        if (x[0] > x[1]) {
            temp = x[0];
            x[0] = x[1];
            x[1] = temp;
        }
        in[0] = (double)x[0];
        in[1] = (double)x[1];
        
        // predict and calculate expected results
        Predict(nn, in, out);
        errVals[0] = out[0] - OP1(in[0], in[1]);
        errVals[1] = out[1] - OP2(in[0], in[1]);
        errVals[2] = out[2] - OP3(in[0], in[1]);
        
        // adjust error
        avgErr = ((1 - alpha) * avgErr) + (alpha * ((errVals[0] * errVals[0]) + (errVals[1] * errVals[1]) + (errVals[2] * errVals[2])));
        ++i;
        
        // print
        printf("i = %u, Error AVG = %lf: Inputs: [%d %d], Outputs (OR, XOR, AND): [%lf %lf %lf], Errors: [%lf %lf %lf]\n", i, avgErr, x[0], x[1], out[0], out[1], out[2], errVals[0], errVals[1], errVals[2]);
        
        // train
        Train(nn, DetermineLearnRate(i), errVals);
    } while ((avgErr > maxError) || (i < minIterations)); 
    
    return;
}

void TestMyNetwork(NeuralNet *nn) {
    int flag;
    int x[2];
    double in[2];
    double out[3];
    int temp;
    
    while (1) {
        printf("Enter two input integers to test: ");
        flag = 2 - scanf("%d%d", &x[0], &x[1]);
        if (x[0] > x[1]) {
            temp = x[0];
            x[0] = x[1];
            x[1] = temp;
        }
        in[0] = (double)x[0];
        in[1] = (double)x[1];
        
        if (flag) {
            return;
        }
        
        Predict(nn, in, out);
        
        printf("OR = %lf, XOR = %lf, AND = %lf\n\n", out[0], out[1], out[2]);
    }
    
    return;
}

int RandomInput(unsigned int i) {
    if ((rand() % 2) == 0) {
        return (rand() % 4);
        /*if ((rand() % 5) == 0) {
            return 0;
        }
        return (rand() % 10);*/
    }

    return (rand() % (INPUT_MAX + 1 - INPUT_MIN)) + INPUT_MIN;
}

double DetermineLearnRate(unsigned int i) {
    // arbitrary
    if (i < 10000) {
        if (i < 1000) {
            return 1.0;
        } else {
            return 0.7;
        }
    } else {
        return 0.4;
    }
}
